import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  addTodos,
  removeTodos,
  setDefault,
  updateTodos,
} from "../redux/todosReducer";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import TodoItem from "./TodoItem";
import Typography from "@mui/material/Typography"

const DisplayTodos = (props) => {
  const [sortSelesai, setSortSelesai] = useState([]);
  const [sortBelumSelesai, setSortBelumSelesai] = useState([]);
  useEffect(()=>{
    if(props.todos){
      var tempSortSelesai = [];
      var tempSortBelumSelesai = [];
      props.todos.map((row)=>{
        if(row.status === 0 || row.status === false){
          tempSortBelumSelesai = [
            ...tempSortBelumSelesai,
            {
              ...row,
              createdAt: new Date(row.createdAt)
            }
          ]
        } else if(row.status === 1 || row.status === true){
          tempSortSelesai = [
            ...tempSortSelesai,
            {
              ...row,
              createdAt: new Date(row.createdAt)
            }
          ]
        }
      })
      setSortBelumSelesai(tempSortBelumSelesai.sort((date1, date2) => date1.createdAt - date2.createdAt))
      setSortSelesai(tempSortSelesai.sort((date1, date2) => date2.createdAt - date1.createdAt))
    }
  },[props.todos])
  return (
    <div
      style={{
        margin: "0 auto",
        width: "100%",
        textAlign: "center"
      }}
    >
     <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Box
            sx={{
            width: "80%",
            height: 400,
            marginLeft: "20%",
            overflowY: "auto",
            border: '4px solid gray'
          }}
          >
          <Typography variant="h6" component="div" gutterBottom align="center">Belum Selesai</Typography>
          {props.todos.length > 0
            ? sortBelumSelesai.map((item) => {
              return (
                <TodoItem
                  key={item.id}
                  item={item}
                  removeTodo={props.removeTodo}
                  updateTodo={props.updateTodo}
                  completeTodo={props.completeTodo}
                /> 
                )
              })
            : null}
          </Box>
        </Grid>
        <Grid item xs={6}>
          <Box
            sx={{
            width: "80%",
            height: 400,
            marginRight: "20%",
            overflowY: "auto",
            border: '4px solid green'
          }}
          > 
          <Typography variant="h6" component="div" gutterBottom align="center">Selesai</Typography>
          {props.todos.length > 0
            ? sortSelesai.map((item) => {
              return (
                <TodoItem
                  key={item.id}
                  item={item}
                  removeTodo={props.removeTodo}
                  updateTodo={props.updateTodo}
                  completeTodo={props.completeTodo}
                />
                )
              })
            : null}
          </Box>
        </Grid>
      </Grid>
    </Box>
    </div>
  );
};

const mapStateToProps = (state) => {
    return {
      todos: state,
    };
  };
  
const mapDispatchToProps = (dispatch) => {
    return {
      addTodo: (obj) => dispatch(addTodos(obj)),
      removeTodo: (id) => dispatch(removeTodos(id)),
      updateTodo: (obj) => dispatch(updateTodos(obj)),
      setDefault: (obj) => dispatch(setDefault(obj)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayTodos);
// import React, { useState } from "react";
// import { connect } from "react-redux";
// import {
//   addTodos,
//   completeTodos,
//   removeTodos,
//   updateTodos,
// } from "../redux/reducer/todosReducer";
// import TodoItem from "./TodoItem";
// import { AnimatePresence, motion } from "framer-motion";

// const mapStateToProps = (state) => {
//   return {
//     todos: state,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     addTodo: (obj) => dispatch(addTodos(obj)),
//     removeTodo: (id) => dispatch(removeTodos(id)),
//     updateTodo: (obj) => dispatch(updateTodos(obj)),
//     completeTodo: (id) => dispatch(completeTodos(id)),
//   };
// };

// const DisplayTodos = (props) => {
//   const [sort, setSort] = useState("active");
//   return (
//     <div className="displaytodos">
//       <div className="buttons">
//         <motion.button
//           whileHover={{ scale: 1.1 }}
//           whileTap={{ scale: 0.9 }}
//           onClick={() => setSort("active")}
//         >
//           Active
//         </motion.button>
//         <motion.button
//           whileHover={{ scale: 1.1 }}
//           whileTap={{ scale: 0.9 }}
//           onClick={() => setSort("completed")}
//         >
//           Completed
//         </motion.button>
//         <motion.button
//           whileHover={{ scale: 1.1 }}
//           whileTap={{ scale: 0.9 }}
//           onClick={() => setSort("all")}
//         >
//           All
//         </motion.button>
//       </div>
//       <ul>
//         <AnimatePresence>
//           {props.todos.length > 0 && sort === "active"
//             ? props.todos.map((item) => {
//                 return (
//                   item.completed === false && (
//                     <TodoItem
//                       key={item.id}
//                       item={item}
//                       removeTodo={props.removeTodo}
//                       updateTodo={props.updateTodo}
//                       completeTodo={props.completeTodo}
//                     />
//                   )
//                 );
//               })
//             : null}
//           {/* for completed items */}
//           {props.todos.length > 0 && sort === "completed"
//             ? props.todos.map((item) => {
//                 return (
//                   item.completed === true && (
//                     <TodoItem
//                       key={item.id}
//                       item={item}
//                       removeTodo={props.removeTodo}
//                       updateTodo={props.updateTodo}
//                       completeTodo={props.completeTodo}
//                     />
//                   )
//                 );
//               })
//             : null}
//           {/* for all items */}
//           {props.todos.length > 0 && sort === "all"
//             ? props.todos.map((item) => {
//                 return (
//                   <TodoItem
//                     key={item.id}
//                     item={item}
//                     removeTodo={props.removeTodo}
//                     updateTodo={props.updateTodo}
//                     completeTodo={props.completeTodo}
//                   />
//                 );
//               })
//             : null}
//         </AnimatePresence>
//       </ul>
//     </div>
//   );
// };

// export default connect(mapStateToProps, mapDispatchToProps)(DisplayTodos);
