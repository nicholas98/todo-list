import React, { useState } from "react";
import { connect } from "react-redux";
import { addTodos } from "../redux/todosReducer";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

const Todos = (props) => {
  
  // console.log("props",props);
  
  const [todo, setTodo] = useState("");
  
    const add = () => {
    if (todo === "") {
      alert("Input is Empty");
    } else {
      props.addTodo({
        createdAt: `2019-11-${Math.floor(Math.random() * 31)} 08:00`,
        description: "lorem ipsum",
        status: 0,
        title: todo,
        id: Math.floor(Math.random() * 1000),
      });
      setTodo("");
    }
  };
  

  const handleChange = (e) => {
    setTodo(e.target.value);
  };

  return (
    <div
      style={{
        margin: "0 auto",
        width: "100%",
        textAlign: "center"
      }}
    >
      <TextField
        type="text"
        onChange={(e) => handleChange(e)}
        className="todo-input"
        placeholder="Insert new Todo"
        value={todo}
      />

      <Button variant="contained" style={{ margin: '8px 0px 0px 12px' }} onClick={() => add()}>
        Add
      </Button>
      <br />
      
      <ul>
        {/* {props.todos.length > 0 &&
          props.todos.map((item) => {
            return <li key={item.id}>{item.item}</li>;
          })} */}
      </ul>
        
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    todos: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: (obj) => dispatch(addTodos(obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Todos);
// import React, { useState } from "react";
// import { connect } from "react-redux";
// import { addTodos } from "../redux/reducer/todosReducer";
// import { GoPlus } from "react-icons/go";
// import { motion } from "framer-motion";

// const mapStateToProps = (state) => {
//   return {
//     todos: state,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     addTodo: (obj) => dispatch(addTodos(obj)),
//   };
// };

// const Todos = (props) => {
//   const [todo, setTodo] = useState("");

//   const handleChange = (e) => {
//     setTodo(e.target.value);
//   };

//   const add = () => {
//     if (todo === "") {
//       alert("Input is Empty");
//     } else {
//       props.addTodo({
//         id: Math.floor(Math.random() * 1000),
//         item: todo,
//         completed: false,
//       });
//       setTodo("");
//     }
//   };
//   //console.log("props from store", props);
//   return (
//     <div className="addTodos">
//       <input
//         type="text"
//         onChange={(e) => handleChange(e)}
//         className="todo-input"
//         value={todo}
//       />
      
//       <motion.button
//         whileHover={{ scale: 1.1 }}
//         whileTap={{ scale: 0.9 }}
//         className="add-btn"
//         onClick={() => add()}
//       >
//         <GoPlus />
//       </motion.button>
//       <br />
//     </div>
//   );
// };
// //we can use connect method to connect this component with redux store
// export default connect(mapStateToProps, mapDispatchToProps)(Todos);
