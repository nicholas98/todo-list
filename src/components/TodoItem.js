import React from "react";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import DetailPage from "./DetailPage";

const TodoItem = (props) => {
  
  const { item, updateTodo, removeTodo } = props;

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
      <List style={{ padding: '4px', border: '1px solid black', margin: '8px', }}>
        <Button onClick={()=>handleClickOpen()}>
          <ListItem>
            <Grid container justifyContent={'space between'}>
              <Grid xs={12}>
                <ListItemText primary={item.title} />
              </Grid>
            </Grid>
          </ListItem>
        </Button>
      <DetailPage
        open={open}
        handleClose={()=> handleClose()}
        removeTodo={removeTodo}
        updateTodo={updateTodo}
        item={item}
      />
      </List>
  );
};

export default TodoItem;