import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import TextField from '@mui/material/TextField';
import MenuItem from '@mui/material/MenuItem';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const BootstrapDialogTitle = (props) => {
  const { children, onClose, ...other } = props;

  return (
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

export default function DetailPage(props) {
    const { open, handleClose, item, removeTodo, updateTodo } = props
    // console.log(item)
    
  const [valueDate, setValueDate] = React.useState(JSON.parse(JSON.stringify(item.createdAt)).split('').filter(row=> row !== "Z").join(''));
  const [title, setTitle] = React.useState(item.title);
  const [description, setDescription] = React.useState(item.description);
  const [statusUpdate, setStatusUpdate] = React.useState(parseInt(item.status));

  const handleUpdate = (e) => {
    console.log(e, statusUpdate, description, title)
    updateTodo({
        createdAt: valueDate,
        description: description,
        id: e.id,
        status: statusUpdate,
        title: title,
    })
    handleClose();
  };
  
  const handleChange = (event) => {
    console.log(event.target.name);
    if(event.target.name === 'Status') {
        setStatusUpdate(event.target.value);
    } else if(event.target.name === 'Description'){
        setDescription(event.target.value);
    } else if(event.target.name === 'Title'){
        setTitle(event.target.value);
    } else {
        setValueDate(event.target.value)
    }
  };
  return (
    <div>
        <BootstrapDialog
            onClose={handleClose}
            aria-labelledby="customized-dialog-title"
            open={open}
        >
            <DialogContent dividers>
            <div style={{ padding: 8 }}>
                <TextField
                    id="outlined-basic"
                    label="Title"
                    variant="outlined"
                    name="Title"
                    onChange={handleChange}
                    fullWidth
                    defaultValue={item.title}
                />
            </div>
            <div style={{ padding: 8 }}>
                <TextField
                    id="outlined-basic"
                    label="Description"
                    variant="outlined"
                    name="Description"
                    fullWidth
                    onChange={handleChange}
                    defaultValue={item.description}
                />
            </div>
            <div style={{ padding: 8 }}>
                <TextField
                    id="outlined-select-currency"
                    select
                    label="Status"
                    name="Status"
                    fullWidth
                    defaultValue={parseInt(item.status)}
                    onChange={handleChange}
                    >
                        <MenuItem value={1}>
                            Selesai
                        </MenuItem>
                        <MenuItem value={0}>
                            Belum Selesai
                        </MenuItem>
                </TextField>
            </div>
            <div style={{ padding: 8 }}>
                <input
                    type="datetime-local"
                    id="createdAt"
                    name="createdAt"
                    onChange={handleChange}
                    defaultValue={valueDate}
                />
            </div>
            </DialogContent>
            <DialogActions>
            <Button autoFocus onClick={() => handleUpdate(item)}>
                Update
            </Button>
            {parseInt(item.status) === 1 ? (
                null
            ) : (
                <Button autoFocus onClick={() => removeTodo(item.id)}>
                    Delete
                </Button>
            )}
            </DialogActions>
        </BootstrapDialog>
    </div>
  );
}
