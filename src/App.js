import DisplayTodos from "./components/DisplayTodos";
import Todos from "./components/Todos";
import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import { useEffect } from "react";
import { setDefault } from "./redux/todosReducer";
import { connect } from "react-redux";
import axios from "axios";

function App(props) {
  useEffect(()=>{
    axios.get('https://virtserver.swaggerhub.com/hanabyan/todo/1.0.0/to-do-list')
    .then((res)=>{
      if(res && props.todos.length === 0){
        props.setDefault([...res.data]);
      }
    })
  },[])
  return (
    <div className="App">
      <Box sx={{
        width: '100%',
        height: '100%',
      }}>
        <Typography variant="h1" component="div" gutterBottom align="center">Todo App</Typography>
        <Todos />
        <DisplayTodos />
      </Box>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    todos: state,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setDefault: (obj) => dispatch(setDefault(obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);