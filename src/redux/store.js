import { configureStore } from "@reduxjs/toolkit";
import { reducer } from "./todosReducer";

const store = configureStore({
    reducer: reducer,
});

export default store;
