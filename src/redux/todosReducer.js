import { createSlice } from "@reduxjs/toolkit";

const initialState = [];
const addTodoReducer = createSlice({
  name: "todos",
  initialState,
  reducers: {
    //here we will write our reducer
    setDefault: (state, action) => {
      console.log(state, action.payload)
      state.push(...action.payload);
      return state;
    },
    //Adding todos
    addTodos: (state, action) => {
      console.log(state, action.payload)
      state.push(action.payload);
      return state;
    },
    //remove todos
    removeTodos: (state, action) => {
      return state.filter((item) => item.id !== action.payload);
    },
    //update todos
    updateTodos: (state, action) => {
      return state.map((todo) => {
        console.log(todo.id, action.payload)
        if (todo.id === action.payload.id) {
          console.log(todo.id, action.payload)
          return {
            ...action.payload,
          };
        }
        return todo;
      });
    },
  },
});

export const {
  addTodos,
  removeTodos,
  updateTodos,
  setDefault,
} = addTodoReducer.actions;
export const reducer = addTodoReducer.reducer;